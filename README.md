# harrytszBlogPics

#### 介绍

本仓库为 harrytsz 多个博客系统提供图床服务

#### 软件架构

可能很多小伙伴苦恼于 Github 搭建的图床访问速度过慢，同时受累于租赁七牛云、阿里云服务器搭建图床步骤的繁琐。为了解决这个痛点，本文提供一套操作流程简洁明了的解决方案：**PicGo + Gitee** 

PicGo + Gitee 图床 只需要将图片上传至指定图床，就可以获取图片链接，适用于支持 Markdown 的各种文档。

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_101725.png)


#### 安装教程

1. 安装 gitee 插件

选择左边栏 "插件设置"，输入"gitee"，出现如下选项，任选一个点击 "安装" 即可。

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_102146.png)

> 需要注意的是，本地必须先安装 [node.js](https://nodejs.org/en/) 才能安装此插件，没有安装 node.js 的小伙伴请先安装 node.js。 

2. 建立 gitee 图床库

没有码云账号的小伙伴，自行注册账号。

点击右上角的 "+" 号，新建仓库：

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_103341.png)


新建仓库的要点如下：   

- 输入一个仓库名称
- 其次将仓库设为公开
- 勾选使用Readme文件初始化这个仓库

**这个选项勾上，这样码云会自动给你的仓库建立master分支，这点很重要!!!**

 因为使用 Github 做图床 Picgo 好像会自动帮你生成 master 分支，而 Picgo 里的 Gitee 插件不会帮你自动生成分支。

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_103932.png)

点击创建进行入下一步

3. 配置 PicGO 

安装了gitee-uploader 1.1.2插件之后，我们开始配置插件配置插件的要点如下：

<img src="https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_104517.png" data-caption="" data-size="normal" data-rawwidth="1002" data-rawheight="565" data-default-watermark-src="https://picb.zhimg.com/50/v2-ca77a80513bcecd4f1dd3c14448e3d96_hd.jpg?source=1940ef5c" class="origin_image zh-lightbox-thumb" width="1002" data-original="https://pic3.zhimg.com/v2-7fd17e45105b65c13c9c7e260a5b6d87_r.jpg?source=1940ef5c"/>


repo：用户名/仓库名称，比如我自己的仓库 harrytsz/harrytszBlogPics     

branch：分支，这里写上master   

path：路径，一般写上img   

customPath：提交消息，这一项和下一项customURL都不用填。

在提交到码云后，会显示提交消息，插件默认提交的是 Upload 图片名 by picGo - 时间

token：填入码云的私人令牌，这个token怎么获取，下面登录进自己的码云点击头像，进入设置

1. 点击右上角头像，选择 "设置"：

![](https://picb.zhimg.com/80/v2-09207edcefff7852c91abcc3df3c5ba0_1440w.jpg?source=1940ef5c)

2. 在左边栏中找到 "安全设置" 里的 "私人令牌"：

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_110000.png)

点击 "生成新令牌"，把 projects 这一项选上，其他的不用勾选，然后提交即可：

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_110252.png)

这里需要验证一下密码，验证密码之后会出来一串数字，这一串数字就是你的token，将这串数字复制到刚才的配置里面去。

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_110809.png)

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_111014.png)


注意：这个令牌只会明文显示一次，建议在配置插件的时候再来生成令牌，直接复制进去，搞丢了又要重新生成一个。

现在保存你刚才的配置，然后将它设置为默认图床，大功告成。


还有一个插件gitee 1.2.2-beta，功能差不多，刚才那个能用的话就不需要用这个，配置的内容有点差别，简单说一下：

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/v22222.jpg)


url：图床网站，这里写码云的主页 https://gitee.com   

owner：所有者

token：刚才你获取的个人令牌，两个插件是通用的，如果你用了另一个再来用这个，它会自动读取另一个插件的部分配置，不用重新申请

message：不用填

4. 测试

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_101725.png)

点击 "上传区"，弹出文件选择框，选择需要上传的图片即可自动上传。上传成功后，会有上传成功的提示信息。

此时，就可以在 "相册" 中查看上传过的图片：

![](https://gitee.com/harrytsz/harrytszBlogPics/raw/master/img/2020-07-22_112311.png)

点击红色箭头所指的按钮，就会自动复制图片的 url ，此时，完成了图床的全部功能。

